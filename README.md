# ModelingApproachXAI

This repository saves the Figures information about the UML and its application for the paper "A Modeling Approach for Designing Explainable Artificial Intelligence" (athors: Álvaro Navarro, Ana Lavalle, Alejandro Maté and Juan Trujillo) presented to the "Companion Proceedings of the 42nd International Conference on Conceptual Modeling: **ER Forum**, 7th SCME, Project Exhibitions, Posters and Demos, and Doctoral Consortium, November 06-09, 2023, Lisbon, Portugal."

